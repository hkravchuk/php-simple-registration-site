<?php
// заголовки, щоб файл приймав тільки JSON
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET, POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// імпорт необхідних класів
include_once 'classes/database.php';
include_once 'classes/user.php';

// підключення до бази
$database = new Database();
$db = $database->getConnection();
$user = new User($db);

// запит інформації про користувача по id
$user->id = $_POST['id'];
$result_array = $user->getProfile();

// якщо результат запиту не пустий, передати інформацію на фронтенд
if (count($result_array)>0) {
    http_response_code(200);
    echo json_encode($result_array);
}
// якщо не вдалося отримати з бази інформацію про користувача
else {
    http_response_code(400);
    echo json_encode(array("message" => "Profile information cannot be displayed. Please contact the administrator."));
}
