<?php
// заголовки, щоб файл приймав тільки JSON
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET, POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// імпорт необхідних класів
include_once 'classes/database.php';
include_once 'classes/user.php';

// підключення до бази
$database = new Database();
$db = $database->getConnection();
$user = new User($db);

// отримання даних з фронтенду
$data = json_decode(file_get_contents("php://input"));

$user->email = $data->email;
$user->login = $data->login;
$user->password = $data->password;
$user->confirm_password = $data->confirm_password;
$user->address = $data->address;
$user->phone = $data->phone;
$user->gender = $data->gender;
$check_email_exists = $user->notExistEmail();
$check_login_exists = $user->loginExists();

// якщо email вже існує в базі
if (!$check_email_exists){
    http_response_code(400);
    echo json_encode(array("message" => "This email is already registered. Please, try again."));
}

// якщо login вже існує в базі
elseif ($check_login_exists){
    http_response_code(400);
    echo json_encode(array("message" => "This login is already taken. Please, try again."));
}

// якщо пароль не співпадає з підтвердженням пароля
elseif ($user->password !== $user->confirm_password){
    http_response_code(400);
    echo json_encode(array("message" => "Password doesn't match. Please, try again."));
}

// реєстрація користувача
elseif ( !empty($user->login) && !empty($user->email) && !empty($user->password) &&
    $user->createAccount() ) {
    http_response_code(200);
    echo json_encode(array("message" => "Registration was successful. Go to your profile."));
}

// якщо не вдалося зареєструвати користувача з інших причин
else {
    http_response_code(400);
    echo json_encode(array("message" => "Unable to register. Please contact the administrator."));
}
