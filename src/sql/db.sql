CREATE DATABASE IF NOT EXISTS app_db;

USE app_db;

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'admin';

CREATE TABLE IF NOT EXISTS users
(
    id serial auto_increment,
    email varchar(255) not null,
    login varchar(255) not null,
    password text not null,
    address text null,
    phone varchar(12) null,
    gender char null,
    created datetime default NOW() not null,
    active boolean default true not null,
    delete_time datetime null,
    constraint table_name_pk
        PRIMARY KEY (id)
);
