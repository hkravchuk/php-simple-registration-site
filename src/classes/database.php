<?php

// клас для підключення до бази
class Database {

    private $host = "localhost";
    private $db_name = "app_db";
    private $username = "root";
    private $password = "admin";
    public $conn;

    // підключення до бази
    public function getConnection() {

        $this->conn = null;

        // PDO клас для роботи з базою
        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
        } catch(PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->conn;
    }
}
?>