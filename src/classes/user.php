<?php

// клас для користувача
class User {

    // підключення до бази
    private $conn;
    private $table_name = "users";

    // властивості користувача
    public $id;
    public $email;
    public $login;
    public $password;
    public $confirm_password;
    public $address;
    public $phone;
    public $gender;

    // конструктор класу, підключення до бази
    public function __construct($db) {
        $this->conn = $db;
    }

    // метод для реєстрації нового користувача
    function createAccount() {

        $query = "INSERT INTO " . $this->table_name . "
                SET
                    email = :email,
                    login = :login,
                    password = :password,
                    address = :address,
                    phone = :phone,
                    gender = :gender";

        // підготовка запиту
        $stmt = $this->conn->prepare($query);

        // захист від sql-ін'єкції
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->login=htmlspecialchars(strip_tags($this->login));
        $this->password=htmlspecialchars(strip_tags($this->password));
        $this->address=htmlspecialchars(strip_tags($this->address));
        $this->phone=htmlspecialchars(strip_tags($this->phone));
        $this->gender=htmlspecialchars(strip_tags($this->gender));

        // хешування пароля для перед записом в базу
        $password_hash = password_hash($this->password, PASSWORD_BCRYPT);

        // прив'язка значень до змінних запиту
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':login', $this->login);
        $stmt->bindParam(':password', $password_hash);
        $stmt->bindParam(':address', $this->address);
        $stmt->bindParam(':phone', $this->phone);
        $stmt->bindParam(':gender', $this->gender);

        // виконання запиту до бази на вставку даних
        if($stmt->execute()) {
            return true;
        }
        return false;
    }

    // метод для перевірки при реєстрації чи вже існує такий email в базі серед активних користувачів
    function notExistEmail(){

        $query = "SELECT id, email
            FROM " . $this->table_name . "
            WHERE active = 1 AND email = :email ";

        // підготовка запиту
        $stmt = $this->conn->prepare($query);

        // захист від sql-ін'єкції
        $this->email=htmlspecialchars(strip_tags($this->email));

        // прив'язка значень до змінних запиту
        $stmt->bindParam(':email', $this->email);

        // виконання запиту до бази на читання даних, кількість рядків у відповіді (0 або 1)
        $stmt->execute();
        $num = $stmt->rowCount();

        if ($num==0){
            return true;
        }
        return false;

    }

    // метод для перевірки при логуванні чи такий користувач вже зареєстрований
    function loginExists(){

        $query = "SELECT id, login, password
            FROM " . $this->table_name . "
            WHERE login = ? AND active = 1
            LIMIT 0,1";

        // підготовка запиту
        $stmt = $this->conn->prepare($query);

        // захист від sql-ін'єкції
        $this->login=htmlspecialchars(strip_tags($this->login));

        // прив'язка значення до змінної запиту
        $stmt->bindParam(1, $this->login);

        // виконання запиту до бази на читання даних, кількість рядків у відповіді (0 або 1)
        $stmt->execute();
        $num = $stmt->rowCount();

        if($num==1) {
            // отримати значення з результату запиту
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->id = $row['id'];
            $this->login = $row['login'];
            $this->password = $row['password'];

            return true;
        }
        return false;
    }

    // метод для відобарження користувачеві інформації його профілю
    function getProfile(){

        $query = "SELECT email, login, address, phone, gender, created
            FROM " . $this->table_name . "
            WHERE id = ?
            LIMIT 0,1";

        // підготовка запиту
        $stmt = $this->conn->prepare($query);

        // захист від sql-ін'єкції
        $this->id=htmlspecialchars(strip_tags($this->id));

        // прив'язка значення до змінної запиту
        $stmt->bindParam(1, $this->id);

        // виконання запиту до бази на читання даних, кількість рядків у відповіді (0 або 1)
        $stmt->execute();
        $num = $stmt->rowCount();

        if($num==1) {
            // отримати значення з результату запиту
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $result_user = array(
                'email' => $row['email'],
                'login' => $row['login'],
                'address' => $row['address'],
                'phone' => $row['phone'],
                'gender' => $row['gender'],
                'created' => $row['created'],
            );
            return $result_user;
        }
        // якщо в базі немає такого користувача - пустий масив
        return [];
    }

    // метод для видалення профіля користувача
    function deleteAccount(){

        $query = "UPDATE " . $this->table_name . "
            SET delete_time = :time, active = 0 
            WHERE id = :id";

        // підготовка запиту
        $stmt = $this->conn->prepare($query);

        // захист від sql-ін'єкції
        $this->id=htmlspecialchars(strip_tags($this->id));
        $time_now = date_create()->format('Y-m-d H:i:s');

        // прив'язка значень до змінних запиту
        $stmt->bindParam(':id', $this->id);
        $stmt->bindParam(':time', $time_now);

        // виконання запиту, повертає true/false (1/0)
        $check = $stmt->execute();

        if ($check==1){
            return true;
        }
        return false;

    }

}