<?php
// заголовки, щоб файл приймав тільки JSON
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET, POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// імпорт необхідних класів
include_once 'classes/database.php';
include_once 'classes/user.php';

// підключення до бази
$database = new Database();
$db = $database->getConnection();
$user = new User($db);

// отримання даних з фронтенду
$data = json_decode(file_get_contents("php://input"));

$user->login = $data->login;
$login_exists = $user->loginExists();

// якщо існує логін та співпадає пароль
if ( $login_exists && password_verify($data->password, $user->password) ) {

    // запам'ятати залогіненого користувача на 5 хвилин (запис id в cookie)
    $current_user = $user->id;
    setcookie("id", $current_user, time()+300);

    http_response_code(200);
    echo json_encode(array("message" => "Successful login."));
}
// якщо логін відсутній в базі або невірний парль
else {
    http_response_code(400);
    echo json_encode(array("message" => "Invalid login or password. Please try again."));
}
