<?php
// заголовки, щоб файл приймав тільки JSON
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET, POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// імпорт необхідних класів
include_once 'classes/database.php';
include_once 'classes/user.php';

// підключення до бази
$database = new Database();
$db = $database->getConnection();
$user = new User($db);

// отримання даних з фронтенду
$user->id = $_POST['id'];

// видалення профіля користувача
if ($user->deleteAccount()){
    http_response_code(200);
    echo json_encode(array("message" => "Account deleted successfully."));
}
// якщо не вдалося видалити профіль користувача
else {
    http_response_code(401);
    echo json_encode(array("message" => "Unable to delete the account. Please contact the administrator."));
}
