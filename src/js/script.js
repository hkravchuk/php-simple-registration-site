jQuery(function($) {


    /* Page index.html*/

    // виконання коду при відправці форми
    $(document).on('submit', '#login_form', function(){

        // отримати дані з форми, перетворити в json
        const sign_up_form = $(this);
        const form_data = JSON.stringify(sign_up_form.serializeObject());

        // відправка даних форми на бекенд
        $.ajax({
            url: "login_user.php",
            type : "POST",
            contentType : 'application/json',
            data : form_data,
            success : function(result) {

                // якщо запит успішний, вивести повідомлення та очистити поля вводу
                sign_up_form.find('input').val('');
                // document.getElementById("correct_massage").innerHTML = result.message;
                // setTimeout(function () {
                    window.location.replace("account.html");
                // }, 1500);
            },
            error: function(xhr, resp, text){

                // невдала реєстрація, повідомлення для користувача
                sign_up_form.find('input').val('');
                document.getElementById("incorrect_massage").innerHTML = xhr.responseJSON.message;
                setTimeout(function () {
                    window.location.replace("/");
                }, 3000);
            }
        });
        return false;
    });

    // функція для перетворення даних форми у формат json
    $.fn.serializeObject = function(){

        const o = {};
        const a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };


    /* Page registration.html */

    // для перевірки нажатого прапорця "I agree" та відображення кнопки submit
    $('#checkbox_check').on('change', function () {
        if ( $('#checkbox_check').prop('checked') ) {
            $('.form__submit').attr('disabled', false);
        } else {
            $('.form__submit').attr('disabled', true);
        }
    });

    // виконання коду при відправці форми
    $(document).on('submit', '#sign_up_form', function(){

        // отримати дані з форми, перетворити в json
        const sign_up_form = $(this);
        const form_data = JSON.stringify(sign_up_form.serializeObject());

        // відправка даних форми на бекенд
        $.ajax({
            url: "create_user.php",
            type : "POST",
            contentType : 'application/json',
            data : form_data,
            success : function(result) {

                // якщо запит успішний, вивести повідомлення та очистити поля вводу
                sign_up_form.find('input').val('');
                document.getElementById("correct_massage").innerHTML = result.message;
                setTimeout(function () {
                    window.location.replace("/");
                }, 1500);
            },
            error: function(xhr, resp, text){

                // невдала реєстрація, повідомлення для користувача
                sign_up_form.find('input').val('');
                document.getElementById("incorrect_massage").innerHTML = xhr.responseJSON.message;
                setTimeout(function () {
                    window.location.replace("registration.html");
                }, 3000);
            }
        });
        return false;
    });


});