jQuery(function($) {

    /* Page account.html */

    // отримати id користувача з cookie
    function getCookie(cname) {
        const name = cname + "=";
        const decodedCookie = decodeURIComponent(document.cookie);
        const ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    // змінити cookie (для видалення сесії користувача)
    function setCookie(cname, cvalue, exseconds) {
        const d = new Date();
        d.setTime(d.getTime() + exseconds);
        const expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires;
    }

    // id поточного залогіненого користувача
    const id_user = getCookie('id');

    // передати id користувача на бекенд, отримати дані профілю користувача
    if (id_user !== "" && id_user != null) {

        $.post("account_user.php", {id: id_user}).done(function (result) {

            $("#result_block")
                .append("Email: " + result.email + "<br>")
                .append("Login: " + result.login + "<br>")
                .append("Address: " + result.address + "<br>")
                .append("Phone: " + result.phone + "<br>")
                .append("Gender: " + result.gender + "<br>")
                .append("Registration date: " + result.created + "<br>");
        }, "json")
            .fail(function (result) {
                document.getElementById("incorrect_massage").innerHTML = "Profile information cannot be displayed.";
            })
    }
    // якщо вкладку /account.html відкриє напряму незалогінений користувач
    else {
        document.getElementById("incorrect_massage").innerHTML = "Please log in.";
        setTimeout(function () {
            window.location.replace("/");
        }, 2000);
    }

    // вихід з профілю користувача
    $(document).on('click', '#logout_button', function () {
        setCookie('id', '', 0);
        // document.getElementById("correct_massage").innerHTML = "You are logout.";
        // setTimeout(function () {
            window.location.replace("/");
        // }, 1500);
    });

    // видалення профілю користувача
    $(document).on('click', '#delete_button', function () {

        let isDelete = confirm("Do you really want to delete your account?");
        if (isDelete) {
            $.post("delete_user.php", {id: id_user}).done(function (result) {
                setCookie('id', '', 0);
                document.getElementById("correct_massage").innerHTML = result.message;
                setTimeout(function () {
                    window.location.replace("/");
                }, 1500);
            })
                .fail(function (result) {
                    document.getElementById("incorrect_massage").innerHTML = result.message;
                    setTimeout(function () {
                        window.location.replace("account.html");
                    }, 3000);
                })
        }
        return false;
    });

});