<?php

namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use PHPUnit\Framework\TestCase;

class ExamTest extends TestCase
{

    public const HOST = 'http://localhost:4444/wd/hub';

    /**
     * @RemoteWebDriver
     */
    public static $driver;


    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $capabilities = DesiredCapabilities::chrome();
        self::$driver = RemoteWebDriver::create(self::HOST, $capabilities);
        self::$driver->get('https://accounts.crowdin.com/login');
    }

    public function testLogin()
    {
        self::$driver->findElement(WebDriverBy::id('login_login'))->sendKeys('galina.k');
        self::$driver->findElement(WebDriverBy::id('login_password'))->sendKeys('galina.k01');
        self::$driver->findElement(WebDriverBy::id('login-container'))->submit();

        $message = self::$driver->findElement(WebDriverBy::className('mb-2'))->getText();
        $this->assertStringContainsString('Want to stay logged in?', $message);

        self::$driver->findElement(WebDriverBy::id('remember-me'))->click();
        $actualUrl = self::$driver->getCurrentURL();
        $this->assertEquals('https://crowdin.com/profile', $actualUrl);
    }

    /**
     * @depends testLogin
     */
    public function testCreateProject()
    {
        self::$driver->findElement(WebDriverBy::id('projects-menu-item'))->click();

        sleep(2);
        self::$driver->findElement(WebDriverBy::cssSelector('li > a[href="/createproject"]'))->click();

        self::$driver->wait(5, 100)->until(
            WebDriverExpectedCondition::urlIs('https://crowdin.com/createproject')
        );

        $randomSuffix = $this->generateRandomString();
        $projectName = 'Test project-' . $randomSuffix;
        self::$driver->findElement(WebDriverBy::id('project_name'))->sendKeys($projectName);

        $radiosElement = self::$driver->findElement(WebDriverBy::id('join-policy-open'));
        $radios = new WebDriverRadios($radiosElement);
        $radios->selectByValue('open');

        self::$driver->findElement(WebDriverBy::className('chosen-single'))->click();
        self::$driver->findElement(WebDriverBy::className('chosen-search'));
        self::$driver->getKeyboard()->sendKeys('Ukrainian');
        self::$driver->findElement(WebDriverBy::cssSelector('li[data-option-array-index="296"]'))->click();
        sleep(1);

        self::$driver->findElement(WebDriverBy::cssSelector('input[data-id="58"]'))->click();
        self::$driver->findElement(WebDriverBy::cssSelector('input[data-id="24"]'))->click();

        self::$driver->findElement(WebDriverBy::id('create_project'))->click();

        $expectedUrl = 'https://crowdin.com/project/';

        self::$driver->wait(5, 100)->until(
            WebDriverExpectedCondition::urlContains($expectedUrl)
        );

        $this->assertStringContainsString(strtolower($randomSuffix), self::$driver->getCurrentURL());

        self::$driver->wait(5, 100)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('a.crowdin_choose_files'))
        );

        $expectedBottomText = 'Upload Files';
        $actualBottomText = self::$driver->findElement(WebDriverBy::cssSelector('a.crowdin_choose_files'))->getText();
        $this->assertSame($expectedBottomText, $actualBottomText);
    }

    /**
     * @depends testCreateProject
     */
    public function testDeleteProject()
    {
        self::$driver->findElement(WebDriverBy::cssSelector('li[data-tab="settings"]'))->click();
        self::$driver->findElement(WebDriverBy::id('delete-project'))->click();

        self::$driver->findElement(WebDriverBy::cssSelector('input#test_project'))->click();
        sleep(3);

        self::$driver->findElement(WebDriverBy::id('btn-remove-project'))->click();

        sleep(3);
        $this->assertSame('https://crowdin.com/profile', self::$driver->getCurrentURL());

    }

    function generateRandomString($length = 8): string {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        self::$driver->quit();
    }
}