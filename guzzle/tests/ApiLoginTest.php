<?php

use GuzzleHttp\Client;

class ApiClientTest extends  PHPUnit\Framework\TestCase {

    protected $httpClient;

    public function setUp(): void
    {
        $this->httpClient = new Client(['base_uri' => 'http://localhost:8001/']);
    }

    public function tearDown(): void
    {
        $this->httpClient = null;
    }

    public function testSuccessLogin()
    {
        $data_input = ['json' => [
            'login' => 'test',
            'password' => '123'
            ]
        ];

        $response = $this->httpClient->get('login_user.php', $data_input);
        $this->assertEquals(200, $response->getStatusCode());

        $data_expected = json_encode(['message' => 'Successful login.']);
        $data_result = json_encode(json_decode($response->getBody(), true));
        $this->assertJsonStringEqualsJsonString($data_expected, $data_result);
    }

    public function testInvalidLogin()
    {
        $data_input = ['json' => [
            'login' => 'test',
            'password' => '000'
            ]
        ];

        try {
            $this->httpClient->get('login_user.php', $data_input);
        }
        catch (GuzzleHttp\Exception\ClientException $e)
        {
            $response = $e->getResponse();
            $this->assertEquals(400, $response->getStatusCode());

            $data_expected = json_encode(['message' => 'Invalid login or password. Please try again.']);
            $data_result = $response->getBody()->getContents();
            $this->assertJsonStringEqualsJsonString($data_expected, $data_result);
        }
    }
}


//
//Notes:
//$response = $e->getResponse();
//var_dump($response->getStatusCode()); // HTTP status code;
//var_dump($response->getReasonPhrase()); // Response message;
//var_dump((string) $response->getBody()); // Body, normally it is JSON;
//var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
//var_dump($response->getHeaders()); // Headers array;
//var_dump($response->hasHeader('Content-Type')); // Is the header presented?
//var_dump($response->getHeader('Content-Type')[0]); // Concrete header value;