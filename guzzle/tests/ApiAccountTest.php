<?php

use GuzzleHttp\Client;

class ApiClientTest extends  PHPUnit\Framework\TestCase
{

    protected $httpClient;

    public function setUp(): void
    {
        $this->httpClient = new Client(['base_uri' => 'http://localhost:8001/']);
    }

    public function tearDown(): void
    {
        $this->httpClient = null;
    }

    public function testGetAccountInformation()
    {
        $data_input = ['form_params' => ['id' => '18']];
        $response = $this->httpClient->request('POST', 'account_user.php', $data_input);
        $this->assertEquals(200, $response->getStatusCode());

        $expected = '{"email":"test@qmail.com","login":"test","address":"Ternopil","phone":"55-555","gender":"F","created":"2021-11-17 13:42:51"}';
        $actual = json_encode(json_decode($response->getBody(), true));
        $this->assertSame($expected, $actual);
    }

    public function testInvalidAccountInformation()
    {
        $data_input = ['form_params' => ['id' => '']];

        try {
            $this->httpClient->request('POST', 'account_user.php', $data_input);
        }
        catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $response = $e->getResponse();
            $this->assertEquals(400, $response->getStatusCode());

            $expected = json_encode(['message' => 'Profile information cannot be displayed. Please contact the administrator.']);
            $actual = $response->getBody()->getContents();
            $this->assertJsonStringEqualsJsonString($expected, $actual);
        }
    }

}