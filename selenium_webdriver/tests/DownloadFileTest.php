<?php

namespace Facebook\WebDriver;

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use PHPUnit\Framework\TestCase;

require_once('vendor/autoload.php');

// download file
class DownloadFileTest extends TestCase {

    public const HOST = 'http://localhost:4444/wd/hub';

    /**
     * @RemoteWebDriver
     */
    public static $driver;
    public static $inputElement;


    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $options = new ChromeOptions();
        $prefs = array('download.default_directory' => '/home/halyna/Downloads');
        $options->setExperimentalOption('prefs', $prefs);
        $capabilities = DesiredCapabilities::chrome();
        $capabilities->setCapability(ChromeOptions::CAPABILITY, $options);
        self::$driver = RemoteWebDriver::create(self::HOST, $capabilities);

        self::$driver->get('https://support.crowdin.com/api/v2/');
        self::$inputElement = WebDriverBy::xpath('/html/body/div/div/div[3]/div[1]/div/div/p/a');
    }

    public function testDownloadFile()
    {
        // чекати завантаження сторінки
        self::$driver->wait(10, 100)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(self::$inputElement)
        );

        $link = self::$driver->findElement(self::$inputElement);
        $href = $link->getAttribute('href');
        self::$driver->get($href);

        // час на скачування файлу
//        self::$driver->manage()->timeouts()->implicitlyWait(7);
        sleep(3);

        // порівняння скачаного файлу з еталоном по хеш-сумі
        $fileExpected = file_get_contents('/home/halyna/Downloads/crowdin_examples.yml');
        $md5_expected = md5($fileExpected);
        $fileActual = file_get_contents('/home/halyna/Downloads/crowdin.yml');
        $md5_actual = md5($fileActual);
        $this->assertEquals($md5_expected, $md5_actual);
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        self::$driver->quit();
    }
}
