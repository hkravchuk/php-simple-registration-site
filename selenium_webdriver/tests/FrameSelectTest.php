<?php

namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use PHPUnit\Framework\TestCase;

require_once('vendor/autoload.php');

// iframe, select
class FrameSelectTest extends TestCase {

    public const HOST = 'http://localhost:4444/wd/hub';

    /**
     * @RemoteWebDriver
     */
    public static $driver;
    public static $inputElement;
    public static $frameElement;
    public static $expectedResult;


    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $capabilities = DesiredCapabilities::chrome();
        self::$driver = RemoteWebDriver::create(self::HOST, $capabilities);
        self::$driver->get('https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_optgroup');

        self::$frameElement = WebDriverBy::id('iframeResult');
        self::$inputElement = WebDriverBy::xpath('//*[@id="cars"]');
        self::$expectedResult = 'audi';
    }

    public function testSelectAndFrame()
    {
        // перейти до iFrame
        $myFrame = self::$driver->findElement(self::$frameElement);
        self::$driver->switchTo()->frame($myFrame);

        // вибрати з випадайки елемент по значенні
        $selectElement = self::$driver->findElement(self::$inputElement);
        $selects = new WebDriverSelect($selectElement);
        $selects->selectByValue(self::$expectedResult);

        $actualSelected = self::$driver->findElement(self::$inputElement)->getAttribute('value');
        $this->assertEquals(self::$expectedResult, $actualSelected);
    }

    public static function tearDownAfterClass(): void
    {self::$driver->findElement(self::$inputElement);
        parent::tearDownAfterClass();
        self::$driver->quit();
    }
}
