<?php

namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\LocalFileDetector;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use PHPUnit\Framework\TestCase;

require_once('vendor/autoload.php');

// upload file
class UploadFileTest extends TestCase {

    public const HOST = 'http://localhost:4444/wd/hub';

    /**
     * @RemoteWebDriver
     */
    public static $driver;
    public static $inputElement;
    public static $outputElement;
    public static $filePath;
    public static $expectedUrl;


    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $capabilities = DesiredCapabilities::chrome();
        self::$driver = RemoteWebDriver::create(self::HOST, $capabilities);
        self::$driver->get('https://www.file.io/');

        self::$inputElement = WebDriverBy::id('upload-button');
        self::$outputElement = WebDriverBy::id('download-url');
        self::$filePath = '/home/halyna/Downloads/apple.webp';
        self::$expectedUrl = 'https://file.io/';
    }

    public function testUploadFile()
    {
        // чекати завантаження сторінки
        self::$driver->wait(10, 100)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(self::$inputElement)
        );

        self::$driver->findElement(self::$inputElement)->sendKeys(self::$filePath);

        // чекати вивантаження файлу
        self::$driver->wait(10, 100)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(self::$outputElement)
        );

        $actual_url = self::$driver->findElement(self::$outputElement)->getText();
        $this->assertStringContainsString(self::$expectedUrl, $actual_url);
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        self::$driver->quit();
    }
}
