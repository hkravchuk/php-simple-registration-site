<?php

namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use PHPUnit\Framework\TestCase;

//require_once('vendor/autoload.php');

// input, wait
class InputWaitTest extends TestCase {

    public const HOST = 'http://localhost:4444/wd/hub';

    /**
     * @RemoteWebDriver
     */
    public static $driver;
    public static $inputElement;
    public static $checkOutputElement;
    public static $inputText;
    public static $expectedOutputText;


    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $capabilities = DesiredCapabilities::chrome();
        self::$driver = RemoteWebDriver::create(self::HOST, $capabilities);
        self::$driver->get('https://translate.google.com/?hl=uk&sl=en&tl=uk&op=translate');

        self::$inputElement = self::$driver->findElement(WebDriverBy::className('er8xn'));
        self::$checkOutputElement = WebDriverBy::className('J0lOec');
        self::$inputText = 'Hi';
        self::$expectedOutputText = 'Привіт';
    }

    public function testInputAndWait()
    {
        self::$inputElement->sendKeys(self::$inputText);

        // очікування перекладу
        self::$driver->wait(5, 1000)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(self::$checkOutputElement)
        );

        $uk = self::$driver->findElement(self::$checkOutputElement)->getText();
        $this->assertStringContainsString(self::$expectedOutputText, $uk);
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        self::$driver->quit();
    }
}
