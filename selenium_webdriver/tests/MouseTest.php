<?php

namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use PHPUnit\Framework\TestCase;

require_once('vendor/autoload.php');

// mouse
class MouseTest extends TestCase {

    public const HOST = 'http://localhost:4444/wd/hub';

    /**
     * @RemoteWebDriver
     */
    public static $driver;
    public static $inputElement;
    public static $resultElement;
    public static $expectedMassage;


    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $capabilities = DesiredCapabilities::chrome();
        self::$driver = RemoteWebDriver::create(self::HOST, $capabilities);
        self::$driver->get('https://uk-ua.facebook.com/');

        self::$inputElement = WebDriverBy::name('login');
        self::$resultElement = WebDriverBy::className('_9ay7');
        self::$expectedMassage = "Указана вами електронна адреса чи номер мобільного телефону не пов'язані із жодним обліковим записом.";
    }

    public function testMouseClick()
    {
        // нажати на кнопку login кліком мишки
        $element = self::$driver->findElement(self::$inputElement);
        self::$driver->getMouse()->mouseMove($element->getCoordinates());
        self::$driver->getMouse()->click();

        $actualMassage = self::$driver->findElement(self::$resultElement)->getText();
        $this->assertStringContainsStringIgnoringCase(self::$expectedMassage, $actualMassage);
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        self::$driver->quit();
    }
}
