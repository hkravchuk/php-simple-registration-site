<?php

namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use PHPUnit\Framework\TestCase;

require_once('vendor/autoload.php');

// keyboard
class KeyboardTest extends TestCase {

    public const HOST = 'http://localhost:4444/wd/hub';

    /**
     * @RemoteWebDriver
     */
    public static $driver;
    public static $inputElement;
    public static $resultElement;
    public static $inputText;
    public static $expectedOutputText;


    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $capabilities = DesiredCapabilities::chrome();
        self::$driver = RemoteWebDriver::create(self::HOST, $capabilities);
        self::$driver->get('https://en.wikipedia.org/wiki/Main_Page');

        self::$inputElement = WebDriverBy::id('searchInput');
        self::$resultElement = WebDriverBy::id('content');
        self::$inputText = 'aloha';
        self::$expectedOutputText = 'ALOHA';
    }

    public function testKeyboard()
    {
        // нажати SHIFT + ввести текст
        self::$driver->findElement(self::$inputElement)->click();
        self::$driver->getKeyboard()->pressKey(WebDriverKeys::SHIFT);
        self::$driver->getKeyboard()->sendKeys(self::$inputText);

        $actualOutput = self::$driver->findElement(self::$inputElement)->getAttribute('value');
        $this->assertEquals(self::$expectedOutputText, $actualOutput);
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        self::$driver->quit();
    }
}
