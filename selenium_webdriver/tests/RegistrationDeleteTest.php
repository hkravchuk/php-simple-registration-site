<?php

namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use PHPUnit\Framework\TestCase;

require_once('vendor/autoload.php');

// depends, button, checkbox, radio box, input, wait, alert
class RegistrationDeleteTest extends TestCase {

    public const HOST = 'http://localhost:4444/wd/hub';

    /**
     * @RemoteWebDriver
     */
    public static $driver;
    public static $emailElement;
    public static $loginElement;
    public static $passwordElement;
    public static $confirmPasswordElement;
    public static $addressElement;
    public static $genderElement;
    public static $checkboxElement;
    public static $submitElement;
    public static $messageElement;
    public static $deleteButtomElement;
    public static $email;
    public static $login;
    public static $password;
    public static $confirmPassword;
    public static $address;
    public static $gender;
    public static $message;
    public static $expectedBody;
    public static $urlLogin;
    public static $urlAccount;
    public static $deleteAlerMassage;
    public static $messageCorrectDelete;


    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $capabilities = DesiredCapabilities::chrome();
        self::$driver = RemoteWebDriver::create(self::HOST, $capabilities);
        self::$driver->get('http://192.168.6.241:8001/registration.html');

        self::$emailElement = WebDriverBy::name('email');
        self::$loginElement = WebDriverBy::id('login');
        self::$passwordElement = WebDriverBy::id('password');
        self::$confirmPasswordElement = WebDriverBy::id('confirm_password');
        self::$addressElement = WebDriverBy::name('address');
        self::$genderElement = WebDriverBy::name('gender');
        self::$checkboxElement = WebDriverBy::id('checkbox_check');
        self::$submitElement = WebDriverBy::name('submit');
        self::$messageElement = WebDriverBy::id('correct_massage');
        self::$deleteButtomElement = WebDriverBy::id('delete_button');

        self::$email = "test_m@gmail.com";
        self::$login = 'test_m';
        self::$password = 'password_m';
        self::$confirmPassword = 'password_m';
        self::$address = 'webdriver test';
        self::$gender = 'M';
        self::$message = 'Registration was successful. Go to your profile.';
        self::$expectedBody = "YOUR ACCOUNT\nEmail: test_m@gmail.com\nLogin: test_m";
        self::$deleteAlerMassage = 'Do you really want to delete your account?';
        self::$messageCorrectDelete = 'Account deleted successfully.';

        self::$urlLogin = 'http://192.168.6.241:8001/';
        self::$urlAccount = 'http://192.168.6.241:8001/account.html';
    }

    public function testRegistration()
    {
        self::$driver->findElement(self::$emailElement)->sendKeys(self::$email);
        self::$driver->findElement(self::$loginElement)->sendKeys(self::$login);
        self::$driver->findElement(self::$passwordElement)->sendKeys(self::$password);
        self::$driver->findElement(self::$confirmPasswordElement)->sendKeys(self::$confirmPassword);
        self::$driver->findElement(self::$addressElement)->sendKeys(self::$address);

        // radio box, вибір елемента за значенням
        $radiosElement = self::$driver->findElement(self::$genderElement);
        $radios = new WebDriverRadios($radiosElement);
        $radios->selectByValue(self::$gender);

        // checkbox, вибір елемента за індексом
        $checkboxesElement = self::$driver->findElement(self::$checkboxElement);
        $checkboxes = new WebDriverCheckboxes($checkboxesElement);
        $checkboxes->selectByIndex(0);

        // перевірка чи checkbox нажатий
        if ($checkboxesElement->isSelected()) {
            self::$driver->findElement(self::$submitElement)->submit();
        }
        else {
            $this->fail();
        }

        // чекати повідомлення про реєстрацію
        self::$driver->wait(5, 100)->until(
            WebDriverExpectedCondition::elementTextIs(self::$messageElement, self::$message)
        );
        $actualMessage = self::$driver->findElement(self::$messageElement)->getText();
        $this->assertEquals(self::$message, $actualMessage);

        // чекати завантаження сторінки
        self::$driver->wait(5, 100)->until(
            WebDriverExpectedCondition::urlIs(self::$urlLogin)
        );
        $actualUrl = self::$driver->getCurrentURL();
        $this->assertEquals(self::$urlLogin, $actualUrl);
    }

    /**
     * @depends testRegistration
     */
    public function testLogin()
    {
        self::$driver->findElement(self::$loginElement)->sendKeys(self::$login);
        self::$driver->findElement(self::$passwordElement)->sendKeys(self::$password);
        self::$driver->findElement(self::$submitElement)->submit();

        // чекати завантаження сторінки
        self::$driver->wait(5, 100)->until(
            WebDriverExpectedCondition::urlIs(self::$urlAccount)
        );
        $actualUrl = self::$driver->getCurrentURL();
        $this->assertEquals(self::$urlAccount, $actualUrl);

        // перевірка вмісту сторінки
        $actual_body = self::$driver->findElement(WebDriverBy::tagName('body'))->getText();
        $this->assertStringContainsString(self::$expectedBody, $actual_body);
    }

    /**
     * @depends testLogin
     */
    public function testTryDeleteAccount()
    {
        $this->pressDeleteButton();

        // відхилити confirm
        self::$driver->switchTo()->alert()->dismiss();
        $actualUrl = self::$driver->getCurrentURL();
        $this->assertEquals(self::$urlAccount, $actualUrl);
    }

    /**
     * @depends testTryDeleteAccount
     */
    public function testDeleteAccount()
    {
        $this->pressDeleteButton();

        // прийняти confirm
        self::$driver->switchTo()->alert()->accept();

        // чекати повідомлення про видалення профілю
        self::$driver->wait(5, 100)->until(
            WebDriverExpectedCondition::elementTextIs(self::$messageElement, self::$messageCorrectDelete)
        );
        $actualMessage = self::$driver->findElement(self::$messageElement)->getText();
        $this->assertEquals(self::$messageCorrectDelete, $actualMessage);

        // чекати завантаження сторінки
        self::$driver->wait(5, 100)->until(
            WebDriverExpectedCondition::urlIs(self::$urlLogin)
        );
        $actualUrl = self::$driver->getCurrentURL();
        $this->assertEquals(self::$urlLogin, $actualUrl);
    }

    public function pressDeleteButton()
    {
        self::$driver->findElement(self::$deleteButtomElement)->click();

        // перевірка елемента confirm
        self::$driver->wait(5, 200)->until(
            WebDriverExpectedCondition::alertIsPresent()
        );
        $text_alert = self::$driver->switchTo()->alert()->getText();
        $this->assertSame(self::$deleteAlerMassage, $text_alert);
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        self::$driver->quit();
    }
}
