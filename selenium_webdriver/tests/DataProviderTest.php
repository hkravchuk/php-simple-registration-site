<?php

namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use PHPUnit\Framework\TestCase;

require_once('vendor/autoload.php');

// dataProvider, input, wait
class DataProviderTest extends TestCase {

    public const HOST = 'http://localhost:4444/wd/hub';

    /**
     * @RemoteWebDriver
     */
    public static $driver;
    public static $inputElement;
    public static $checkOutputElement;


    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $capabilities = DesiredCapabilities::chrome();
        self::$driver = RemoteWebDriver::create(self::HOST, $capabilities);
    }

    /**
     * @dataProvider inputProvider
     */
    public function testDataProviderInput($inputText, $expectedOutputText)
    {
        // переклад en -> uk
        self::$driver->get('https://translate.google.com/?hl=uk&sl=en&tl=uk&op=translate');
        self::$driver->findElement(WebDriverBy::className('er8xn'))->sendKeys($inputText);

        // очікування перекладу
        self::$driver->wait(5, 200)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('J0lOec'))
        );

        $uk = self::$driver->findElement(WebDriverBy::className('J0lOec'))->getText();
        $this->assertStringContainsString($expectedOutputText, $uk);
    }

    // набір $inputText, $expectedOutputText
    public function inputProvider(): array
    {
        return [
            ['Hi', 'Привіт'],
            ['how are you', 'як справи'],
            ['class', 'клас'],
            ['function', 'функція'],
        ];
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        self::$driver->quit();
    }
}
