<?php

class RegistrationDeleteTest extends \PHPUnit\Extensions\Selenium2TestCase
{

    public static function setUpBeforeClass(): void
    {
        self::shareSession(true);
    }

    public function setUp(): void
    {
        $this->setBrowserUrl('http://localhost:8001/');
        $this->setBrowser('chrome');
        $this->setDesiredCapabilities(['chromeOptions' => ['w3c' => false]]); // phpunit-selenium does not support W3C mode yet
    }

    public function testRegistration()
    {
        $login = 'test_n';
        $password = 'password_n';
        $this->url('/registration.html');
        $this->byName('email')->value('test_n@gmail.com');
        $this->byName('login')->value($login);
        $this->byId('password')->value($password);
        $this->byId('confirm_password')->value($password);
        $this->byName('address')->value('selenium test');
        $this->byName('phone')->value('');
        $this->byXPath('//*[@id="sign_up_form"]/div[7]/input[1]')->click();

        $this->byId('checkbox_check')->click();
        $this->byName('submit')->submit();
        sleep(1);

        $correct_massage = $this->byId('correct_massage')->text();
        $this->assertEquals('Registration was successful. Go to your profile.', $correct_massage);

        sleep(2);
        $expected_url = "http://localhost:8001/";
        $this->assertSame($expected_url, $this->url());

        return [$login, $password];
    }

    /**
     * @depends testRegistration
     */
    public function testLogin(array $input)
    {
        $login = $input[0];
        $password = $input[1];

        $this->byId('login')->value($login);
        $this->byId('password')->value($password);
        $this->byName('submit')->submit();
        sleep(1);

        $expected_url = "http://localhost:8001/account.html";
        $this->assertSame($expected_url, $this->url());

        $expected_body = "YOUR ACCOUNT\nEmail: test_n@gmail.com\nLogin: test_n";
        $actual_body = $this->byTag('body')->text();
        $this->assertStringContainsString($expected_body, $actual_body);
    }

    /**
     * @depends testLogin
     */
    public function testTryDeleteAccount()
    {
        $this->pressDeleteButton();

        $this->dismissAlert();
        sleep(1);
        $expected_url = "http://localhost:8001/account.html";
        $this->assertSame($expected_url, $this->url());
    }

    /**
     * @depends testTryDeleteAccount
     */
    public function testDeleteAccount()
    {
        $this->pressDeleteButton();

        $this->acceptAlert();
        sleep(1);
        $correct_massage = $this->byId('correct_massage')->text();
        $this->assertEquals('Account deleted successfully.', $correct_massage);

        sleep(2);
        $expected_url = "http://localhost:8001/";
        $this->assertSame($expected_url, $this->url());
    }

    public function pressDeleteButton()
    {
        $this->byId('delete_button')->click();

        $check_confirmation = $this->alertIsPresent();
        $text_confirmation = $this->alertText();
        $this->assertTrue($check_confirmation);
        $this->assertSame('Do you really want to delete your account?', $text_confirmation);
    }
}
