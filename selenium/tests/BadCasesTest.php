<?php

class BadCasesTest extends \PHPUnit\Extensions\Selenium2TestCase
{

    public string $app_url = 'http://localhost:8001/';
    public string $correct_login = 'test';
    public string $incorrect_password = '000';
    public string $form_field_login = 'login';
    public string $form_field_password = 'password';
    public string $button_confirm_login = 'submit';
    public string $incorrect_massage_text = 'Invalid login or password. Please try again.';

    public static function setUpBeforeClass(): void
    {
        self::shareSession(true);
    }

    public function setUp(): void
    {
        $this->setBrowserUrl('http://localhost:8001/');
        $this->setBrowser('chrome');
        $this->setDesiredCapabilities(['chromeOptions' => ['w3c' => false]]); // phpunit-selenium does not support W3C mode yet
    }

    public function testIncorrectLogin()
    {
        $this->url('');
        $this->byId('login')->value('test');
        $this->byId('password')->value('000');
        $this->byName('submit')->submit();
        sleep(1);

        $incorrect_massage = $this->byId('incorrect_massage')->text();
        $this->assertEquals('Invalid login or password. Please try again.', $incorrect_massage);

        sleep(3);
        $expected_url = "http://localhost:8001/";
        $this->assertSame($expected_url, $this->url());

    }

    /**
    * @dataProvider registrationProvider
    */
    public function testIncorrectRegistration($email, $login, $password, $confirm_password, $address, $phone, $gender, $message)
    {
        $this->url('/registration.html');
        $this->byName('email')->value($email);
        $this->byName('login')->value($login);
        $this->byId('password')->value($password);
        $this->byId('confirm_password')->value($confirm_password);
        $this->byName('address')->value($address);
        $this->byName('phone')->value($phone);
        $this->byXPath($gender)->click();

        $this->byId('checkbox_check')->click();
        $this->byName('submit')->submit();
        sleep(1);
        $incorrect_massage = $this->byId('incorrect_massage')->text();
        $this->assertEquals($message, $incorrect_massage);

        sleep(3);
        $expected_url = "http://localhost:8001/registration.html";
        $this->assertSame($expected_url, $this->url());
    }

    public function registrationProvider(): array
    {
        return [
            ['bad_login@gmail.com', 'bad_login', 'bad_password', '', 'unknown', '', '//*[@id="sign_up_form"]/div[7]/input[1]', "Password doesn't match. Please, try again."],
            ['bad_login@gmail.com', 'bad_login', 'bad_password', 'bad_confirm', 'unknown', '', '//*[@id="sign_up_form"]/div[7]/input[2]', "Password doesn't match. Please, try again."],
            ['bad_login@gmail.com', 'test', 'bad_password', 'bad_password', 'unknown', '0670000000', '//*[@id="sign_up_form"]/div[7]/input[1]', 'This login is already taken. Please, try again.'],
            ['test@qmail.com', 'bad_login', 'bad_password', 'bad_password', 'unknown', '0670000000', '//*[@id="sign_up_form"]/div[7]/input[2]', 'This email is already registered. Please, try again.'],
        ];
    }
}