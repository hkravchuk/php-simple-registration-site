<?php

class TitleTest extends \PHPUnit\Extensions\Selenium2TestCase
{
    public function setUp(): void
    {
        $this->setBrowserUrl('http://localhost:8001');
        $this->setBrowser('chrome');
    }

    public function testLoginTitle()
    {
        $this->url('');
        $this->assertEquals('Login', $this->title());
    }

    public function testRegistrationTitle()
    {
        $this->url('/registration.html');
        $this->assertEquals('Registration', $this->title());
    }

    public function testAccountTitle()
    {
        $this->url('/account.html');
        $this->assertSame('Account', $this->title());
    }

}