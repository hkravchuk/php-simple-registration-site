<?php

class AccountTest extends \PHPUnit\Extensions\Selenium2TestCase
{

    public string $app_url = 'http://localhost:8001/';
    public string $account_url = 'http://localhost:8001/account.html';
    public string $correct_login = 'test';
    public string $correct_password = '123';
    public string $user_information = 'YOUR ACCOUNT\nEmail: test@qmail.com\nLogin: test\nAddress: Ternopil\nPhone: 55-555\nGender: F\nRegistration date: 2021-11-17 13:42:51';
    public string $form_field_login = 'login';
    public string $form_field_password = 'password';
    public string $button_confirm_login = 'submit';
    public string $button_logout = 'logout_button';
    public string $heading_login_tag = 'h2';
    public string $heading_login_text = 'LOGIN';


    public static function setUpBeforeClass(): void
    {
        self::shareSession(true);
    }

    public function setUp(): void
    {
        $this->setBrowserUrl('http://localhost:8001/');
        $this->setBrowser('chrome');
        $this->setDesiredCapabilities(['chromeOptions' => ['w3c' => false]]); // phpunit-selenium does not support W3C mode yet
    }

    public function testLoginAccount()
    {
        $this->url('');
        $this->byId('login')->value('test');
        $this->byId('password')->value('123');
        $this->byName('submit')->submit();
        sleep(1);

        $expected_url = "http://localhost:8001/account.html";
        $this->assertSame($expected_url, $this->url());

        $expected_body = "YOUR ACCOUNT\nEmail: test@qmail.com\nLogin: test\nAddress: Ternopil\nPhone: 55-555\nGender: F\nRegistration date: 2021-11-17 13:42:51";
        $actual_body = $this->byTag('body')->text();
        $this->assertSame($expected_body, $actual_body);
    }

    /**
     * @depends testLoginAccount
     */
    public function testLogout()
    {
        $this->byId('logout_button')->click();
        sleep(1);

        $expected_url = "http://localhost:8001/";
        $this->assertSame($expected_url, $this->url());

        $actual_heading = $this->byTag('h2')->text();
        $this->assertEquals('LOGIN', $actual_heading);
    }
}